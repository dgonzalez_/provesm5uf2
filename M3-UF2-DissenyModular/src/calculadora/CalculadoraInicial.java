package calculadora;

import java.util.Scanner;

	public class CalculadoraInicial {
		
		static Scanner reader = new Scanner(System.in);
		
		public static void main(String[] args) {  //Programa principal. Fa de distribuidor de feines
			// TODO Auto-generated method stub
			int x=0,y=0;
			char opcio;
			int resultat = 0;
			boolean control = false;
			boolean primeraOperacio = true;
			
			do  {
			    opcio = mostrarMenu();
				
				switch(opcio) {
			
		            case 'O': 
			            	if (primeraOperacio == true) {
			            		x=obtenirEnter();
			            	} else {
			            		x = resultat;
			            	}
					        y=obtenirEnter();
					        control = true;
					        break;
					case '+': if (control)
									resultat = suma(x,y);   
							   else mostrarError("Error, per sumar has de posar primer els valors a operar");
							 	primeraOperacio = false; 
							break;
					case '-': if (control)
									resultat = resta(x,y);   
								else mostrarError("Per restar cal llegir primer els valors");
								primeraOperacio = false; 
		                      break;
					case '*': 	if (control)
										resultat = multiplicacio(x,y);   
								else mostrarError("Per multiplicar cal llegir primer els valors");
								primeraOperacio = false; 
								break;
					case '/': if (control) {
								    resultat = divisio(x,y);   
								}
								else mostrarError("Per dividir cal llegir primer els valors");
								primeraOperacio = false; 
							  break;
		            case 'V': if (control)
		            			visualitzar(resultat);
		            		  else mostrarError("Per visualitzar cal llegir primer els valors");
						   	  break;
			 
		            case 'S': System.out.print("Acabem.....");
		                    
		                      break;
					default: System.out.print("opci� erronia");
				}
				
			}while (opcio!='S');
			
			System.out.print("\nAdeu!!!");
			
		}
		
	//Declaraci� dels m�todes	
	public static int obtenirEnter() {  
	
	    int valor = 0;
	    
	    boolean valid = false;
	    
	    do {
	    	try {
		    	System.out.print("Introdueix un valor enter: ");
		    	valor = reader.nextInt();
		    	valid = true;
		    }
		    catch (Exception e) {
		    	System.out.println("Error, cal introduir un valor enter");
		    	reader.nextLine();
		    } 
	    } while (valid == false);
	    
	    reader.nextLine();
	    
	    return valor; 
	}

	public static void visualitzar(int res) {  
		System.out.println("El resultat de l'operaci� �s: " + res);	
	}

	public static char obtenirChar() {
			// TODO Auto-generated method stub
			char c;
			
			System.out.print("Introdueix un car�cter: ");
			c = reader.nextLine().toUpperCase().charAt(0);
			return c;
	}


	public static void mostrarError(String mensaje) {
			System.out.println(mensaje);
	}
			
	public static char mostrarMenu() {
		     	char opcio;
		     	
				System.out.print("\nCalculadora:\n\n");
				System.out.print("\no.- Obtenir els valors");
				System.out.print("\n+.- Sumar");
				System.out.print("\n-.- Restar");
				System.out.print("\n*.- Multiplicar");
				System.out.print("\n/.- Dividir");
				System.out.print("\nv.- Visualitzar resultat");
				System.out.print("\ns.- Sortir");
				System.out.println("\n\nTria una opci�: ");
				opcio = obtenirChar();
				return opcio;
	}
	
	public static int suma (int a, int b) {	
		int res;
		res = a + b;
		return res;
	}

	public static int resta (int a, int b) {	
		int res;
		res = a - b;
		return res;
	}

	public static int multiplicacio (int a, int b) {	
		int res;
		res = a * b;
		return res;
	}

	public static int divisio (int a, int b) {	
		int res = 0;
		char op;
		
		if (b == 0) mostrarError("No es pot dividir entre zero");
		else {
			do{
			   System.out.print("M." + a + " mod " + b);
			   System.out.print("D." + a + " div " + b);
			   op = obtenirChar();
			   if (op == 'M') res = a % b;
			   else if (op == 'D') res = a / b;
			        else mostrarError("opci� incorrecte");
		    } while (op!='M' && op!='D');
		}
		return res;
	}
}
