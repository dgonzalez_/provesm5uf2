package penjat;

import java.util.Scanner;

public class Programa {
	
	static Scanner src = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int opcio;
		String jugador = "";
		Jugador jugadores = new Jugador();
		boolean definido = false;
		boolean ganador;
		
		do {
			opcio = menu();
			switch (opcio) {
				case 1: Ajuda.mostrarInfo();
						break;
				case 2: jugador = Jugador.definirJugador();
						definido = true;
						break;
				case 3: if (definido) {
							ganador = Jugar.jugarPartida(jugador);
							Jugador.actualizar(jugador, ganador);
							definido = false;
						}
						else System.out.println("Error, cal definir primer el jugador");
						break;
				case 4: Jugador.mostrarJugador(jugador);
						break;
				case 5: Jugador.mostrarJugadores();
						break;
				case 0: System.out.println("Moltes gr�cies per jugar\n\nAdeu!!!");
						break;
				default: System.out.println("Error, opci� incorrecta. Torna-ho a provar..");
			}
			
		} while (opcio != 0);
	}

	private static int menu() {
		// TODO Auto-generated method stub
		System.out.println("1.- Mostrar Ajuda");
		System.out.println("2.- Definir Jugador");
		System.out.println("3.- Jugar Partida");
		System.out.println("4.- Veure Jugador");
		System.out.println("5.  Ranking jugadores");
		System.out.println("0.- Sortir");
		System.out.println("\nEscull una opci�: ");
		
		return leerEntero();
		
	}

	private static int leerEntero() {
		// TODO Auto-generated method stub
		int op = 0;
		boolean valid = false;
		
		do {
			try {
				op = src.nextInt();
				valid = true;
			}
			catch (Exception e) {
				System.out.println("Error, has de posar un n�mero enter");
				src.nextLine();
			}
		} while(!valid);
		
		return op;
	}

}
