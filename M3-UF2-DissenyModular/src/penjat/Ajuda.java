package penjat;

public class Ajuda {

	public static void mostrarInfo() {
		// TODO Auto-generated method stub
		System.out.println("****  Instrucciones juego del Ahorcado  ******");
		System.out.println("----------------------------------------------");
		System.out.println("El juego consiste en adivinar una palabra secreta seleccionada por el programa de forma aleatoria.\n");
		System.out.println("Al comienzo del juego, el programa muestra una sucesi�n de guiones. Cada gui�n contiene una letra de la palabra que se ha de adivinar.\n");
		System.out.println("En cada tirada el jugador indica una letra. Si la letra est� dentro de la palabra, se muestra, tantas veces como aparezca, sustituyendo el gui�n correspondiente por la posici�n que ocupa.\n");
		System.out.println("Si la letra indicada por el jugador no est� en la palabra, el jugador tendr� un fallo. Se pierde la partida, si se falla m�s de 10 veces; y se gana si se adivina completamente la palabra sin superar los 10 fallos.\n");
		System.out.println("Mucha suerte y a jugar....\n");

		System.out.println("-----------------------------------------------");
		
	}


}
