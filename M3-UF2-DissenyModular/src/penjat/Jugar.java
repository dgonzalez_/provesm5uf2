package penjat;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Jugar {

	static Scanner src = new Scanner(System.in);
	
	public static boolean jugarPartida(String jugador) {
		// TODO Auto-generated method stub
		
		//Declaraci�n de variables
		ArrayList <String> palabras; //contiene las posibles palabras a adivinar 
		int pos;    				 //indica la posici�n seleccionada, en el ArrayList, de la palabra a adivinar en esa partida
		String palabraAAdivinar;	//palabra seleccionada que ha de adivinar el usuario en esa partida
		//char [] palabraEnProceso;    //vector de car�cteres con la palabra que est� rellenando el usuario. Inicialmente tendr� un gui�n por cada letra
		ArrayList<Character> palabraEnProceso;  //�dem que el anterior vector pero usando arrayList
		int fallos;					//contador de fallos (letra que no se encuentra en la palabra) en la tirada del usuario
		char letra;					//guarda la letra seleccionada por el usuario en cada tirada
		boolean finPartida;			//indica, despu�s de cada tirada, si el usuario a adivinado ya la palabra (true) o si a�n le quedan letras por adivinar (false)
		final int TOTAL_FALLOS; 	//constante que contiene el m�ximo de fallos permitidos para poder ganar la partida
		
		//Inicializaci�n de la partida - Preparaci�n del juego
		palabras = inicializarPalabras();  					//guarda la lista de palabras a adivinar en el ArrayList palabras
		palabraAAdivinar = escogerPalabra(palabras);		//escoger palabra a adivinar aleatoriamente
		//palabraEnProceso = inicializarConGuiones(palabraAAdivinar.length());   //inicializar la palabra en proceso de juego con guiones: palabraEnProceso
		palabraEnProceso = inicializarConGuionesArrayList(palabraAAdivinar.length()); 
		fallos = 0;
		TOTAL_FALLOS = 10;
		finPartida = false;
		boolean encontrado;
		
		//Desarrollo del juego
		
		while (finPartida == false && fallos < TOTAL_FALLOS) {
			mostrar(palabraEnProceso, fallos, TOTAL_FALLOS);
			letra = obtenerLetra();
			encontrado = comprobar(letra, palabraAAdivinar, palabraEnProceso);
			if (encontrado == false) fallos++;
			else 
				finPartida = verificarPalabraCompleta(palabraAAdivinar, palabraEnProceso);
		}
		mostrarResultadoPartida(finPartida, fallos, TOTAL_FALLOS, palabraAAdivinar);
		return (fallos < TOTAL_FALLOS);  //return finPartida;
	}

	private static char[] inicializarConGuiones(int longitud) {
		// TODO Auto-generated method stub
		char [] pal = new char[longitud];
		for (int i = 0; i < longitud; i++)
			pal[i] = '-';
		
		return pal;
	}

	private static String escogerPalabra(ArrayList<String> palabras) {
		// TODO Auto-generated method stub
		Random rnd = new Random();	
		/*
		 * String pal; 
		 * int pos = rnd.nextInt(palabras.size()); 
		 * pal = palabras.get(pos);
		 * pal = pal.toUpperCase(); 
		 * return pal;
		 */
		return palabras.get(rnd.nextInt(palabras.size())).toUpperCase();
	}

	private static ArrayList<String> inicializarPalabras() {
		// TODO Auto-generated method stub
		ArrayList<String> pals = new ArrayList<String>();
		pals.add("Clase");
		pals.add("Programacion");
		pals.add("Ciclo");
		pals.add("Formativo");
		pals.add("Amarillo");
		pals.add("Rojo");
		pals.add("Azul");
		pals.add("Violeta");
		pals.add("Murcielago");
		pals.add("Cocodrilo");
		pals.add("Palanca");
		pals.add("Supercalifragilisticoespialidoso");
		pals.add("Hola");
		pals.add("Adios");
		
		return pals;
	}

	private static void mostrarResultadoPartida(boolean ganaPartida, int fallos, int total, String palAdivinar) {
		// TODO Auto-generated method stub
		char [] pal = palAdivinar.toCharArray();
		System.out.println("\n*** Resultado final de la partida.... ***");
		mostrar(pal, fallos, total);
		if (ganaPartida == true)
			System.out.println("Felicidades campe�n/na, has ganado la partida.\n");
		else
			System.out.println("Lo siento, has perdido. Has de practicar m�s.\n");
		System.out.println("\n*****************************************");
	}

	private static boolean verificarPalabraCompleta(String palAdivinar, char[] palProceso) {
		// TODO Auto-generated method stub
		boolean iguales = true;
		int i = 0;
		while (i < palAdivinar.length() && iguales) {
			if (palAdivinar.charAt(i) != palProceso[i]) iguales = false;
			i++;
		}
		return iguales;
	}

	private static boolean comprobar(char letra, String palAdivinar, char[] palProceso) {
		// TODO Auto-generated method stub
		//Aqu� tenemos la garant�a que la letra y palAdivinar est�n en may�sculas
		boolean encontrado = false;
		
		for (int i = 0; i < palAdivinar.length(); i++) {
			if (letra == palAdivinar.charAt(i)) {
				palProceso[i] = letra;
				encontrado = true;
			}
		}
		return encontrado;
	}

	private static char obtenerLetra() {
		// TODO Auto-generated method stub
		char letra;
		boolean correcto = false;
		
		System.out.print("Letra ?: ");
		do {
			letra = src.nextLine().toUpperCase().charAt(0);
			
			if (letra >= 'A' && letra <= 'Z') correcto = true;
			else
				System.out.println("Error, has de poner una letra del alfabeto ingl�s");
		
		} while (!correcto);
		
		return letra;
	}

	private static void mostrar(char[] palabra, int fallos, int total) {
		// TODO Auto-generated method stub
		System.out.println("\n\n===============================================================");
		System.out.println("M�ximo de fallos permitidos: " + total + " - Fallos acumulados: " + fallos + "\n");
		System.out.println("\n");
		
		for (int i = 0; i < palabra.length; i++) {
			System.out.print(palabra[i] + " ");
		}
		System.out.println("\n");
	}
	
	//M�todos equivalentes a los hechos con PalabraEnProceso como vector de char. Ahora hechos con ArrayList
	
	private static ArrayList<Character> inicializarConGuionesArrayList(int longitud) {
		// TODO Auto-generated method stub
		ArrayList<Character> pal = new ArrayList<Character>();
		for (int i = 0; i < longitud; i++)
			pal.add('-');
		
		return pal;
	}

	private static boolean verificarPalabraCompleta(String palAdivinar, ArrayList<Character> palProceso) {
		// TODO Auto-generated method stub
		boolean iguales = true;
		int i = 0;
		while (i < palAdivinar.length() && iguales) {
			if (palAdivinar.charAt(i) != palProceso.get(i)) iguales = false;
			i++;
		}
		return iguales;
	}

	private static boolean comprobar(char letra, String palAdivinar, ArrayList<Character> palProceso) {
		// TODO Auto-generated method stub
		//Aqu� tenemos la garant�a que la letra y palAdivinar est�n en may�sculas
		boolean encontrado = false;
				
		for (int i = 0; i < palAdivinar.length(); i++) {
			if (letra == palAdivinar.charAt(i)) {
				palProceso.set(i,letra);
				encontrado = true;
			}
		}
		return encontrado;
	}

	private static void mostrar(ArrayList<Character> palProceso, int fallos, int total) {
		// TODO Auto-generated method stub
		System.out.println("\n\n===============================================================");
		System.out.println("M�ximo de fallos permitidos: " + total + " - Fallos acumulados: " + fallos + "\n");
		System.out.println("\n");
		
		for (int i = 0; i < palProceso.size(); i++) {
			System.out.print(palProceso.get(i) + " ");
		}
		System.out.println("\n");
	}

	

}
