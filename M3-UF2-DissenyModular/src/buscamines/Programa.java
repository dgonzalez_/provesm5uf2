package buscamines;

import java.util.Scanner;

/**
 * <h2>Clase Programa, contiene todos los metodos para que el programa funcione </h2>
 * 
 * Busca informaci�n del Bucaminas en <a href="https://buscaminas.eu/">BUSCAMINAS</a>
 * @see <a href="https://buscaminas.eu/">BUSCAMINAS</a>
 * @author David Gonzalez
 * @version 1.0
 * @since 16-02-2021
 *
 */
public class Programa {
	
	public static void main(String[] args) {
		/**
		 * Entrada por teclado
		 */
		Scanner scn = new Scanner(System.in);
		/**
		 * Fin de la partida
		 */
		boolean salir = false;
		/**
		 * Opci�n del menu
		 */
		int opcion; 
		/**
		 * Define el jugador antes de iniciar la partida
		 */
		boolean definido = false;
		/**
		 * Define el ganador de la partida
		 */
		boolean ganador;
		/**
		 * Nombre del jugador
		 */
		String jugador="";
		/**
		 * Nivel de dificultad de la partida
		 */
		int nivel; 
		/**
		 * Numero de filas
		 */
		int filas = 0; 
		/*
		 * Numero de columnas
		 */
		int columnas = 0; 
		/*
		 * Numero de minas
		 */
		int minas = 0; 
		
		
		while(!salir) {
			
			System.out.println("");
			System.out.println("1.-Mostrar Ayuda");
			System.out.println("2.-Opciones");
			System.out.println("3.-Jugar Partida");
			System.out.println("4.-Ver jugador");
			System.out.println("5.-Ver Rankings");
			System.out.println("0.-Salir");
			
			System.out.println("");
			System.out.println("Escribe una de las opciones:");
			opcion = scn.nextInt();
			
			switch (opcion) {
			case 1:
				Ayuda.mostrar();
				break;
			case 2:
				jugador = Jugador.definirJugador();
				nivel = Opciones.obtenerNivelJuego();
				filas = Opciones.obtenerTama�o(nivel, "filas");
				columnas = Opciones.obtenerTama�o(nivel, "columnas");
				minas = Opciones.numeroMinas(nivel, filas, columnas);
				definido = true;
				break;
			case 3:
				if (definido) {
					ganador = Jugar.jugarPartida(jugador, filas, columnas, minas);
					if (ganador == true) {
						System.out.println("\nEnhorabuena has ganado!");
					} else {
						System.out.println("\nVaya, has perdido...");
					}
					if (ganador) {
						Jugador.actualizar(jugador, "gana");
					} else {
						Jugador.actualizar(jugador, "pierde");
						definido = false;
					}
				} else {
					System.out.println("Error, cal definir primer el jugador");
				}
				break;
			case 4:
				Jugador.mostrarJugador(jugador);
				break;
			case 5:
				Jugador.mostrarJugadores();
				break;
			case 0:
				System.out.println("Gracias por jugar, adi�s!");
				salir = true;
				break;
			default:
				System.out.println("Solo n�meros entre 1 y 4");
			}
			
		}
		
	}

}
