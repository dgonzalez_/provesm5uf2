package buscamines;

import java.util.Scanner;

/**
 * <h2>Clase Opciones, elige el nivel de juego</h2>
 * 
 * @author David Gonzalez
 * @version 1.0
 * @since 16-02-2021
 */
public class Opciones {
	/**
	 * Entrada por teclado
	 */
	static Scanner scn = new Scanner(System.in);

	/**
	 * Metodo para obtener el nivel del juego
	 * 
	 * @return nivel de dificultad del juego
	 */
	public static int obtenerNivelJuego() {

		int nivel = 0;
		boolean correcto = false;

		System.out.println("\n****  Bienvenido al juego del Buscaminas  ****\n");
		System.out.println("Indica el nivel de juego: ");
		System.out.println("1. Principiante");
		System.out.println("2. Intermedio");
		System.out.println("3. Experto");
		System.out.println("4. Personalizado");
		System.out.print("\nOpci�n? : ");

		do {
			nivel = leerEntero();
			if (nivel >= 1 && nivel <= 4) {
				correcto = true;
			} else {
				System.out.println("Error, opci�n de nivel incorrecta. Vuelve a probar");
			}
		} while (!correcto);
		return nivel;
	}

	/**
	 * Metodo privado para introducir entero y leer que sea valido
	 * 
	 * @return n�mero para elegir el nivel
	 */
	private static int leerEntero() {
		int opcion = 0;
		boolean valido = false;

		do {
			try {
				opcion = scn.nextInt();
				valido = true;

			} catch (Exception e) {
				System.out.println("Error, debes de poner un numero entero");
				scn.nextLine();
			}
		} while (!valido);

		return opcion;
	}

	/**
	 * Metodo para obtener tama�o del tablero
	 * 
	 * @param nivel   indica el nivel de dificultad de la partida
	 * @param mensaje indica filas o columnas
	 * @return el tama�o del tablero
	 */
	public static int obtenerTama�o(int nivel, String mensaje) {
		/*
		 * Nivel principiante: 8 � 8 casillas y 10 minas. Nivel intermedio: 16 � 16
		 * casillas y 40 minas. Nivel experto: 16 � 30 casillas y 99 minas. Nivel
		 * personalizado: en este caso el usuario personaliza su juego eligiendo el
		 * n�mero de minas y el tama�o de la cuadricula
		 */
		int size = 0;

		switch (nivel) {
		case 1:
			size = 8;
			break;
		case 2:
			size = 16;
			break;
		case 3:
			if (mensaje.equalsIgnoreCase("filas")) {
				size = 16;
			} else {
				size = 30;
			}
			break;
		case 4:
			boolean correcto = false;
			do {
				System.out.println("Indica el numero de " + mensaje + " en tu tablero personalizado");
				size = leerEntero();
				if (size >= 1 && size <= 100) {
					correcto = true;
				} else {
					System.out.println(
							"Error, n�mero de " + mensaje + " incorrecto. El m�ximo permitido es 100. Vuelve a probar");
				}
			} while (!correcto);
		}
		return size;
	}

	/**
	 * Metodo que crea una cantidad de minas dependiendo en el nivel que est�
	 * 
	 * @param nivel    indica el nivel del juego
	 * @param filas    indica las filas del tablero
	 * @param columnas indica las columnas del tablero
	 * @return el numero de minas
	 */
	public static int numeroMinas(int nivel, int filas, int columnas) {
		int minas = 0;

		switch (nivel) {
		case 1:
			minas = 10;
			break;
		case 2:
			minas = 40;
			break;
		case 3:
			minas = 99;
			break;
		case 4:
			boolean correcto = false;
			int maxminas = (int) ((filas * columnas) * 0.2); // el maximo de minas que permitimos es un 20% del total de
																// las casillas del tablero
			do {
				System.out.println("Indica el numero de minas en tu tablero personalizado");
				minas = leerEntero();
				if (minas >= 1 && minas <= maxminas) {
					correcto = true;
				} else {
					System.out.println("Error, n�mero de minas incorrecto . Vuelve a probar");
				}

			} while (!correcto);
		}
		return minas;
	}

}
