package buscamines;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * <h2>Clase Jugador, crea el jugador</h2>
 * @author David
 * @version 1.0
 * @since 16-02-2021
 */
public class Jugador {

	/**
	 * Entrada por teclado
	 */
	static Scanner scn = new Scanner(System.in);
	/**
	 * Lista de nombre de jugadores
	 */
	static ArrayList<String> nomJugadors = new ArrayList<String>();
	/**
	 * Lista de partidas ganadas
	 */
	static ArrayList<Integer> partidasGanadas = new ArrayList<Integer>();
	/**
	 * Lista de partidas perdidas
	 */
	static ArrayList<Integer> partidasPerdidas = new ArrayList<Integer>();

	/**
	 * Metodo que define el jugador con su nombre, partidas ganadas y partidas
	 * perdidas
	 * 
	 * @return nom indica el nombre del jugador
	 */
	public static String definirJugador() {

		String nom;

		System.out.println("Introdueix el nom del Jugador: ");

		nom = scn.nextLine();

		if (nomJugadors.indexOf(nom) == -1) { // Devuelve -1 si no contiene el nombre en el Arraylist
			nomJugadors.add(nom);
			partidasGanadas.add(0);
			partidasPerdidas.add(0);
		}
		return nom;
	}

	/**
	 * Metodo para actualizar las victorias y derrotas del jugador
	 * 
	 * @param jugador indica jugador
	 * @param estado  indica si gana o pierde
	 */
	public static void actualizar(String jugador, String estado) {
		int i = nomJugadors.indexOf(jugador);

		if (i == -1) {
			System.out.println("Error aquest jugador no existeix");
		} else {
			if (estado.contentEquals("gana")) {
				partidasGanadas.set(i, partidasGanadas.get(i) + 1);
			} else {
				partidasPerdidas.set(i, partidasPerdidas.get(i) + 1);

			}
		}
	}

	/**
	 * Metodo para mostrar el jugador
	 * 
	 * @param jugador indica el jugador de la partida
	 */
	public static void mostrarJugador(String jugador) {

		int i = nomJugadors.indexOf(jugador);

		if (i == -1) {
			System.out.println("Error este jugador no existe");
		} else {
			System.out.println("\nDatos del jugador " + nomJugadors.get(i) + "\n");
			System.out.println("Partidas ganadas " + partidasGanadas.get(i));
			System.out.println("Partidas perdidas " + partidasPerdidas.get(i) + "\n");
		}
	}

	/**
	 * Metodo que muestra un ranking de los jugadores que han jugado
	 */
	public static void mostrarJugadores() {
		for (int i = 0; i < nomJugadors.size(); i++) {
			System.out.println("Datos del jugador " + nomJugadors.get(i) + "\n");
			System.out.println("Partidas guanyades: " + partidasGanadas.get(i));
			System.out.println("Partidas perdidas: " + partidasPerdidas.get(i) + "\n");
		}

	}

}
