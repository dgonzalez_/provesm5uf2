package buscamines;


/**
 * <h2>Clase Ayuda, contiene la informaci�n del Buscaminas</h2>
 * 
 * Busca informaci�n del Bucaminas en <a href="https://buscaminas.eu/">BUSCAMINAS</a>
 * 
 * @author David Gonzalez
 * @version 1.0
 * @since 16-02-2021
 */
public class Ayuda {
	
	/**
	 * M�todo que muestra toda la informaci�n de ayuda
	 */
	public static void mostrar() {
		System.out.println(" - En un tauler de mida variable, es col�loca una certa quantitat de mines aleat�riament.\n");
		System.out.println(" - Participa un jugador, descobrint una casella cada torn\n");
		
		System.out.println(" - La casella a destapar pot contenir 3 tipus de valors:\n"
				+ " � Mina: el programa finalitza i la partida s�ha perdut\r\n"
				+ " � N�mero entre 1 i 8: indica quantes caselles contig�es contenen una mina. El jugador pot continuar jugant.\r\n"
				+ " � Buit (aigua): Casella que no t� cap mina al seu voltant. Al destaparla provoca que es destapin autom�ticament totes les caselles contig�es de forma\r\n"
				+ "");
		System.out.println(" - El joc acaba quan les �niques caselles sense descobrir siguin les mines, o b� quan el jugador descobreix una mina (BOOM!!!).\n");
		System.out.println(" - S�intentar� oferir la possibilitat de  que els jugadors siguin humans o autom�tics (ordinador).");
		
	}

}
