package buscamines;

import java.util.Random;
import java.util.Scanner;

/**
 * <h2>Clase Jugar, juega la partida</h2>
 * 
 * Las tiradas se hacen automaticamente, lo he hecho para que cada vez que
 * quiera comprobar si funciona el programa, no tarde tanto al comprobarlo
 * 
 * @author David Gonzalez
 * @version 1.0
 * @since 16-02-2021
 *
 */
public class Jugar {

	/**
	 * Entrada por teclado
	 */
	static private Scanner scn = new Scanner(System.in);

	/**
	 * Valor de las casillas sin descubrir
	 */
	static char TAPADA = '_';

	/**
	 * Valor de la primera casilla que se destapa
	 */
	static char AGUA = ' ';

	/**
	 * Valor de las casillas que contienen minas
	 */
	static char BOMBA = 'O';

	/**
	 * Valor de las casillas que el jugador a descubierto y estaban vacias
	 */
	static char LLENAR = 'A';

	/**
	 * Metodo para jugar partida, desde empezarla hasta que se acabe
	 * 
	 * @param jugador  indica el nombre del jugador
	 * @param filas    indica el n�mero de filas del tablero
	 * @param columnas indica el n�mero de columnas del tablero
	 * @param minas    indica el n�mero de minas colocadas en el tablero
	 * @return
	 *         <ul>
	 *         <li>true: ha ganado la partida</li>
	 *         <li>false: ha perdido la partida</li>
	 *         </ul>
	 */
	public static boolean jugarPartida(String jugador, int filas, int columnas, int minas) {

		System.out.println("Jugador: " + jugador);
		System.out.println("Filas: " + filas);
		System.out.println("Columnas: " + columnas);
		System.out.println("Minas: " + minas);

		/**
		 * Tablero que ve el jugador y que decide que casilla destapar
		 */
		char[][] visible;
		/**
		 * Tablero donde est�n escondidas las minas
		 */
		char[][] oculto;
		/**
		 * Indica si la partida se ha acabado o no
		 */
		boolean finPartida;
		/**
		 * Define el estado del tablero despu�s de cada tirada
		 */
		int estado;
		/**
		 * Total de minas colocadas en el tablero
		 */
		int totalMinas;

		// Iniciarlizar partida
		estado = 1;
		totalMinas = 0;
		finPartida = false;
		visible = inicializarTablero(filas, columnas);
		oculto = ponerMinas(filas, columnas, minas);

		// Desarrollo del juego
		while (!finPartida) {
			mostrar(visible, filas, columnas, minas, totalMinas);
			estado = tirada(visible, oculto, filas, columnas, totalMinas);
			finPartida = estado != 1;
		}

		resultadoPartida(visible, oculto, filas, columnas, estado);
		if (estado != 1) {
			return false;
		}
		return true;
	}

	/**
	 * Dependiendo de las filas y columnas introducidas por parametro crea el
	 * tablero y lo rellena con '_'
	 * 
	 * @param filas    indica el numero de filas del tablero
	 * @param columnas indica el numero de columnas del tablero
	 * @return el tablero visible con '_'
	 */
	private static char[][] inicializarTablero(int filas, int columnas) {

		char[][] visible = new char[filas][columnas];

		for (int i = 0; i < filas; i++) {
			for (int j = 0; j < columnas; j++) {
				visible[i][j] = TAPADA;
			}
		}
		return visible;
	}

	/**
	 * Crea el tablero oculto y coloca las minas en el
	 * 
	 * @param filas    indica el numero de filas del tablero
	 * @param columnas indica el numero de columnas del tablero
	 * @param minas    indica el numero de minas colocadas en el tablero
	 * @return el tablero oculto con las minas ya colocadas
	 */
	private static char[][] ponerMinas(int filas, int columnas, int minas) {

		char[][] oculto = new char[filas][columnas];
		Random rnd = new Random();
		int f;
		int c;

		for (int i = 0; i < filas; i++) {
			for (int j = 0; j < columnas; j++) {
				oculto[i][j] = TAPADA;
			}
		}

		for (int b = 0; b < minas; b++) {
			do {
				f = rnd.nextInt(filas - 1);
				c = rnd.nextInt(columnas - 1);
			} while (oculto[f][c] == BOMBA);
			oculto[f][c] = BOMBA;
		}
		System.out.println("\nMinas puestas");
		return oculto;
	}

	/**
	 * Muestra el tablero visible para que el jugador lo pueda ver
	 * 
	 * @param visible    indica el tablero que ve el jugador
	 * @param filas      indica el numero de filas del tablero
	 * @param columnas   indica el numero de columnas del tablero
	 * @param minas      indica las minas colocadas en el tablero
	 * @param totalMinas indica el total de las minas colocadas en el tablero
	 */
	private static void mostrar(char[][] visible, int filas, int columnas, int minas, int totalMinas) {

		System.out.printf("%4c", ' ');
		for (int j = 0; j < columnas; j++)
			System.out.printf("%4d", j);
		System.out.println();

		for (int i = 0; i < filas; i++) {
			System.out.printf("%4d", i);
			for (int j = 0; j < columnas; j++)
				System.out.printf("%4c", visible[i][j]);
			System.out.println();
		}
	}

	/**
	 * El jugador va haciendo tiradas, por cada casilla que destapa, comprueba que
	 * debe hacer
	 * 
	 * @param visible  indica el tablero que ve el jugador
	 * @param oculto   indica el tablero oculto donde se colocan las minas
	 * @param filas    indica el numero de filas del tablero
	 * @param columnas indica el numero de columnas del tablero
	 * @param minas    indica el numero de minas colocadas en el tablero
	 * @return un numero para comprobar el estado de la partidam, si la partida
	 *         sigue o acaba
	 */
	private static int tirada(char[][] visible, char[][] oculto, int filas, int columnas, int minas) {
		Random rnd = new Random();
		int fila = 0;
		int columna = 0;
		int turno = 1;
		int destapar = 0;
		boolean finPartida = false;
		int comprobar = (filas * columnas) - minas;
		boolean bomba = false;

		while (!finPartida) {
			if (turno == 1) {
				fila = rnd.nextInt(filas);
				columna = rnd.nextInt(columnas);
				descubrir(fila, columna, visible, oculto, filas, columnas);
			}

			fila = rnd.nextInt(filas - 1);
			columna = rnd.nextInt(columnas - 1);

			if (visible[fila][columna] == TAPADA && oculto[fila][columna] != BOMBA) {
				visible[fila][columna] = LLENAR;
				destapar++;
			} else if (oculto[fila][columna] == BOMBA) {
				bomba = true;
				return 0;
			}
			turno++;

			if (bomba == true) {
				finPartida = true;
				return 0;
			} else if (bomba == false && destapar == comprobar) {
				finPartida = true;
			}
		}
		return 1;
	}

	/**
	 * Destapan las casillas del tablero sin contarlas
	 * 
	 * @param fila     indica la fila entrada por teclado
	 * @param columna  indica la columna entrada por teclado
	 * @param visible  indica el tablero que ve el jugador
	 * @param oculto   indica el tablero oculto donde se colocan las minas
	 * @param filas    indica el numero de filas del tablero
	 * @param columnas indica el numero de columnas del tablero
	 */
	private static void descubrir(int fila, int columna, char[][] visible, char[][] oculto, int filas, int columnas) {

		if (fila >= 0 && filas < filas && columna >= 0 && columna < columnas && visible[fila][columna] == TAPADA) {

			visible[fila][columna] = oculto[fila][columna];

			if (oculto[fila][columna] == AGUA) {

				descubrir(fila - 1, columna, visible, oculto, filas, columnas);
				descubrir(fila - 1, columna - 1, visible, oculto, filas, columnas);
				descubrir(fila - 1, columna + 1, visible, oculto, filas, columnas);
				descubrir(fila + 1, columna, visible, oculto, filas, columnas);
				descubrir(fila + 1, columna - 1, visible, oculto, filas, columnas);
				descubrir(fila + 1, columna + 1, visible, oculto, filas, columnas);
				descubrir(fila, columna - 1, visible, oculto, filas, columnas);
				descubrir(fila, columna + 1, visible, oculto, filas, columnas);
			}

		}
	}

	/**
	 * Muestra el resultado de la partida una vez acabada (falta por terminar)
	 * 
	 * @param visible  indica el tablero que ve el jugador
	 * @param oculto   indica el tablero oculto donde se colocan las minas
	 * @param filas    indica el numero de filas del tablero
	 * @param columnas indica el numero de columnas del tablero
	 * @param estado   indica si la partida sigue o debe acabar
	 */
	private static void resultadoPartida(char[][] visible, char[][] oculto, int filas, int columnas, int estado) {
		System.out.println("\n----------------------------");
		System.out.println("| Resultado de la partida: |");
		System.out.println("----------------------------\n");

		mostrar(visible, filas, columnas, columnas, estado);

		if (estado != 1) {
			System.out.println("\nDerrota!");
		} else {
			System.out.println("\nVictoria!");
		}
	}
}
