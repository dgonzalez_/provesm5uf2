package tresenratlla;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Jugar {

	static Scanner src = new Scanner(System.in);
	static final char FICHA_X = 'X', FICHA_O = 'O';   //Ficha asignada a cada jugador 
	
	public static String jugarPartida(String j1, String j2) {
		// TODO Auto-generated method stub
		//Declaraci�n de variables
		
		final int N = 3;						     //Tama�o del tablero cuadrado
		String ganador = null;						 //Contiene el nombre del jugador que gana la partida
		char [][] tablero;							 //Tablero donde se pondr�n las fichas
		int turno;									 //indica a qui�n le toca jugar. 1: jugador1, 2: jugador2
		boolean finPartida;						 	 //indica si la partida ha finalizado o no
		int estado = -1;									 //Define el estado del tablero despu�s de cada tirada
													        //-1: a�n no ha acabado la partida
															//0: la partida ha quedado en tablas
															//1: ha ganado el jugador 1
															//2: ha ganado el jugador 2
		
		//Inicializaci�n de la partida - Preparaci�n del juego
		finPartida = false;
		turno = 1;									 //Comienza el juego el jugador1
		tablero = inicializarTablero(N);             //Pone todas las casillas con '_'
		
		//Desarrollo del juego
		while (!finPartida) {
			mostrar(tablero, N, turno, j1, j2);
			tirada(tablero, N, turno, j1, j2);			//tirada del jugador correspondiente
			estado = comprobarTablero(tablero, N, turno);		//retorna -1 si la partida a�n no ha acabado
			finPartida = (estado != -1);
			turno = siguienteJugador(turno);	
		}
		ganador = resultadoPartida(tablero, N, estado, j1, j2);
		return ganador;  //retorna el nombre del jugador que ha ganado la partida. Si la partida queda en tablas, retornara NULL
	}


	private static char[][] inicializarTablero(int n) {
		// TODO Auto-generated method stub
		char[][] tablero = new char[n][n];
		
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				tablero[i][j] = '_';
		return tablero;
	}

	private static String resultadoPartida(char[][] tablero, int n, int estado, String j1, String j2) {
		// TODO Auto-generated method stub
		//Partida finalizada. La variable estado nos indica: 0: partida en tablas / 1: gana jugador 1 / 2: gana jugador 2
		System.out.println("Partida finalizada:\n");
		mostrar(tablero, n);
		switch(estado) {
			case 0: System.out.println("\nPartida acabada en tablas. No hay ganador\n\n");
				return null;
			case 1: System.out.println("\nFelicidades, " + j1 + ". Has ganado la partida\n\n");
				return j1;
			case 2: System.out.println("\nFelicidades, " + j2 + ". Has ganado la partida\n\n");
				return j2;		
		}
		return null;
	}

	private static int siguienteJugador(int turno) {
		// TODO Auto-generated method stub
		if (turno == 1) return 2;
		return 1;
	}
	
	private static int comprobarTablero(char[][] tablero, int n, int turno) {
		// TODO Auto-generated method stub
		//retorna un entero con : -1 - La partida no ha finalizado / 0: la partida ha quedado en tablas / 1: la partida la ha ganado el jugador 1 / 2: la partida la ha ganado en jugador 2
		
		int i, j;
		boolean ganador = false;
		boolean casillasLibres = false;
		char car;
		
		//comprobamos por filas
		i = 0;
		while (i < n && !ganador) {
			car = tablero[i][0];
		    j = 0;
		    while (j < n && tablero[i][j] == car && tablero[i][j] != '_') j++;

		    if (j == n) ///a la fila j totes les caselles tenen el mateix valor
		        ganador = true;
		    else
		    	if (tablero[i][j] == '_') casillasLibres = true;

		    i++;
		}
		
		//Si ganador continua a falso, comprobamos por columnas
		j = 0;
		while (j < n && ganador == false)
		{
		    car = tablero[0][j];
		    i = 0;
		    while (i < n && tablero[i][j] == car && tablero[i][j] != '_') i++;

		    if (i == n) ///a la columna i totes les caselles tenen el mateix valor
		        ganador = true;
		    else
		    	if (tablero[i][j] == '_') casillasLibres = true;
		    j++;
		}
		
		//Si ganador continua a falso, miramos la diagonal principal
		i = 0;
		car = tablero[0][0];
		while (i < n && tablero[i][i] == car && tablero[i][i] != '_') i++;

		if (i == n) ///a la diagonal principal totes les caselles tenen el mateix valor
		      ganador = true;
		else
	    	if (tablero[i][i] == '_') casillasLibres = true;
		
		//Si ganador continua a falso, miramos la diagonal secundaria
		
		i = 0;
		car = tablero[i][n-1-i];
	    while (i < n && tablero[i][n-1-i] == car && tablero[i][n-1-i] != '_') i++;

	    if (i == n) ///a la diagonal secund�ria totes les caselles tenen el mateix valor
		        ganador = true;
	    else
	    	if (tablero[i][n-1-i] == '_') casillasLibres = true;
	    
	    //resultado a retornar...
		if (ganador)
			return turno;
		
		if (casillasLibres) 
			return -1;
		
		return 0;
	}


	private static void tirada(char[][] tablero, int n, int turno, String j1, String j2) {
		// TODO Auto-generated method stub
		Random rnd = new Random();
		int fila, col;								 //guardan las coordenadas (fila, col) de la casilla seleccionada
		char ficha;
		String j;
		boolean correcto;
		
		if (turno == 1) {
			ficha = FICHA_X;
		}
		else {
			ficha = FICHA_O;
		}
		
	   System.out.println("Pon la ficha " + ficha);
			
			correcto = true;
			do {
				fila = obtenerNumero(n, "Introduce el valor de la fila: "); 
				col = obtenerNumero(n, "Introduce el valor de la columna: ");
				if (tablero[fila][col] != '_') {
					correcto = false;
					System.out.println("Error, casilla ocupada. Vuelve a probar...");
				}
			} while (!correcto);
			
			tablero[fila][col] = ficha;

		
	}

	private static int obtenerNumero(int n, String mensaje) {
		// TODO Auto-generated method stub
		int pos = 0;
		boolean correcto = false;
		
		while (!correcto) {
			try {
				System.out.print(mensaje);
				pos = src.nextInt();
				if (pos >= 0 && pos < n) correcto = true;
				else System.out.println("\nError, valor no v�lido\n");
			} catch (Exception e) {
				System.out.println("\nError, se espera un n�mero entero\n");
				src.nextLine();
			}	
		}
		src.nextLine();
		return pos;
	}

	private static void mostrar(char[][] tablero, int n, int turno, String j1, String j2) {
		// TODO Auto-generated method stub
		mostrar(tablero, n);
		if (turno == 1) 
			System.out.println ("Toca jugar a " + j1 + "\n");
		else 
			System.out.println ("Toca jugar a " + j2 + "\n");
	}
	
	private static void mostrar(char[][] tablero, int n) {
		
		System.out.printf("%4c", ' ');
		for (int j = 0; j < n; j++)
			System.out.printf("%4d", j);
		System.out.println();
		
		for (int i = 0; i < n; i++) {
			System.out.printf("%4d", i);
			for (int j = 0; j < n; j++)
				System.out.printf("%4c", tablero[i][j]);
			System.out.println();
		}
		System.out.println();
	}
	
}
